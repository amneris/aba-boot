package com.abaenglish.boot.autoconfigure.amqp;

import com.abaenglish.boot.amqp.config.dummy.DummyRabbitAdmin;
import com.abaenglish.boot.amqp.config.dummy.DummyRabbitTemplate;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.SimpleRoutingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ConditionalOnClass({RabbitTemplate.class, Channel.class})
@ConditionalOnProperty(prefix = "amqp", name = "dummy", havingValue = "true")
@AutoConfigureBefore({AmqpAutoConfiguration.class, AmqpAdminAutoConfiguration.class})
public class DummyAmqpAutoConfiguration {

    @Bean
    ConnectionFactory dummyConnectionFactory() {
        return new SimpleRoutingConnectionFactory();
    }

    @Bean
    @ConditionalOnProperty(prefix = "spring.rabbitmq", name = "dynamic", havingValue = "true", matchIfMissing = true)
    AmqpAdmin dummyAmqpAdmin() {
        return new DummyRabbitAdmin();
    }

    @Bean
    RabbitTemplate dummyRabbitTemplate(ConnectionFactory connectionFactory) {
        DummyRabbitTemplate rabbitTemplate = new DummyRabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean(name = "rabbitRetryListenerContainerFactory")
    @Primary
    RabbitListenerContainerFactory rabbitRetryListenerContainerFactory() {
        return null;
    }

}
