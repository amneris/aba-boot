package com.abaenglish.boot.autoconfigure.amqp;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ConditionalOnClass({ RabbitTemplate.class, Channel.class })
@EnableConfigurationProperties(AmqpProperties.class)
@ConditionalOnProperty(prefix = "spring.rabbitmq", name = "dynamic", havingValue = "true", matchIfMissing = true)
public class AmqpAdminAutoConfiguration {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    AmqpProperties amqpProperties;

    @PostConstruct
    void adminRabbit() {
        amqpProperties.getExchange().ifPresent(exchange -> amqpAdmin.declareExchange(new TopicExchange(exchange, true, false)));

        for (AmqpProperties.Queue propertyQueue : amqpProperties.getQueues()) {
            TopicExchange exchange = new TopicExchange(propertyQueue.getExchange(), true, false);
            amqpAdmin.declareExchange(exchange);
            for (String routingKey : propertyQueue.getRoutingKeys()) {
                Map<String, Object> args = new HashMap<>();
                args.put("x-dead-letter-exchange", exchange.getName());
                args.put("x-dead-letter-routing-key", propertyQueue.getName() + ".deadletters");
                if (propertyQueue.getTimeout() != null) {
                    args.put("x-message-ttl", propertyQueue.getTimeout());
                }

                Queue queue = new Queue(propertyQueue.getName(), true, false, false, args);

                amqpAdmin.declareQueue(queue);
                amqpAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey));

                Queue queueDeadLetters = new Queue(propertyQueue.getName() + ".deadletters");
                amqpAdmin.declareQueue(queueDeadLetters);
                amqpAdmin.declareBinding(BindingBuilder.bind(queueDeadLetters).to(exchange).with(queueDeadLetters.getName()));
            }
        }
    }

}
