package com.abaenglish.boot.api;

import com.abaenglish.boot.exception.ApiException;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;

import java.util.Map;

@Component
public class AbaDefaultErrorAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(
            RequestAttributes requestAttributes, boolean includeStackTrace) {
        Map<String, Object> attributes = super.getErrorAttributes(requestAttributes, includeStackTrace);

        Throwable error = getError(requestAttributes);
        if (error instanceof ApiException) {
            String abaCode = ((ApiException) error).getCode();
            if (abaCode != null) {
                attributes.put("abaCode", abaCode);
            }

        }

        return attributes;
    }
}

