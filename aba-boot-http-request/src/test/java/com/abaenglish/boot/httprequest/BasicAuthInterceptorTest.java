package com.abaenglish.boot.httprequest;

import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicAuthInterceptorTest {

    private InterceptingClientHttpRequestFactory requestFactory;
    private RequestFactoryMock requestFactoryMock;
    private RequestMock requestMock;
    private ResponseMock responseMock;

    @Before
    public void setUp() throws Exception {
        requestFactoryMock = new RequestFactoryMock();
        requestMock = new RequestMock();
        responseMock = new ResponseMock();
    }

    @Test
    public void testInterceptor() throws IOException, URISyntaxException {
        final String authHeader = "Basic " + new String(Base64.encodeBase64("user:password".getBytes(Charset.forName("US-ASCII"))));

        requestMock = new RequestMock() {
            @Override
            public ClientHttpResponse execute() throws IOException {
                assertThat(getHeaders()).containsKey("Authorization");
                assertThat(authHeader).isIn(getHeaders().get("Authorization"));
                return super.execute();
            }
        };

        BasicAuthInterceptor interceptor = new BasicAuthInterceptor("user", "password");
        requestFactory = new InterceptingClientHttpRequestFactory(requestFactoryMock, Collections.singletonList(interceptor));

        ClientHttpRequest request = requestFactory.createRequest(new URI("http://example.com"), HttpMethod.GET);
        request.execute();
    }

    private static class ResponseMock implements ClientHttpResponse {

        private HttpStatus statusCode = HttpStatus.OK;
        private String statusText = "";
        private HttpHeaders headers = new HttpHeaders();

        @Override
        public HttpStatus getStatusCode() throws IOException {
            return statusCode;
        }

        @Override
        public int getRawStatusCode() throws IOException {
            return statusCode.value();
        }

        @Override
        public String getStatusText() throws IOException {
            return statusText;
        }

        @Override
        public HttpHeaders getHeaders() {
            return headers;
        }

        @Override
        public InputStream getBody() throws IOException {
            return null;
        }

        @Override
        public void close() {
        }
    }

    private class RequestFactoryMock implements ClientHttpRequestFactory {

        @Override
        public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
            requestMock.setURI(uri);
            requestMock.setMethod(httpMethod);
            return requestMock;
        }

    }

    private class RequestMock implements ClientHttpRequest {

        private URI uri;
        private HttpMethod method;
        private HttpHeaders headers = new HttpHeaders();
        private ByteArrayOutputStream body = new ByteArrayOutputStream();
        private boolean executed = false;

        private RequestMock() {
        }

        @Override
        public URI getURI() {
            return uri;
        }

        public void setURI(URI uri) {
            this.uri = uri;
        }

        @Override
        public HttpMethod getMethod() {
            return method;
        }

        public void setMethod(HttpMethod method) {
            this.method = method;
        }

        @Override
        public HttpHeaders getHeaders() {
            return headers;
        }

        @Override
        public OutputStream getBody() throws IOException {
            return body;
        }

        @Override
        public ClientHttpResponse execute() throws IOException {
            executed = true;
            return responseMock;
        }
    }
}
