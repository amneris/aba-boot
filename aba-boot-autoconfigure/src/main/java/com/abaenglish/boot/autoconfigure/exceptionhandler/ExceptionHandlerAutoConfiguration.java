package com.abaenglish.boot.autoconfigure.exceptionhandler;

import com.abaenglish.boot.api.AbaDefaultErrorAttributes;
import com.abaenglish.boot.api.GlobalExceptionHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

@Import({GlobalExceptionHandler.class})
@ConditionalOnClass(GlobalExceptionHandler.class)
public class ExceptionHandlerAutoConfiguration {

    @Bean
    @Primary
    public ErrorAttributes abaErrorAttributes() {
        return new AbaDefaultErrorAttributes();
    }
}
