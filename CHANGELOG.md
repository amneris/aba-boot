# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.6.0] - 2017-04-11
### Added
- Added a new constructor for ServiceException and deprecated old one.
- Added lombok dependency
- Added orika converter of localDate and localDateTime

### Changed
- Skipped javadoc generation on child projects
- Updated spring-boot version from 1.4.1.RELEASE to 1.4.5.RELEASE
- Improve testing with OrikaBeanMapper

## [1.4.7] - 2017-01-11
### Added
- add retryable consumer configuration

## [1.4.6] - 2016-12-01
### Changed
- changed docker base image for microservices.

## [1.4.5] - 2016-11-23
### Added
- added new module (aba-boot-starter-cache) to activate cache.

## [1.4.2] - 2016-10-28
### Fixed
- fixed how docker image tags are created

## [1.4.0] - 2016-10-28
### Added
- updated spring boot version to 1.4.0.RELEASE

## [1.3.14] - 2016-10-13
### Added
- added a new method to set headers to an event

## [1.3.13] - 2016-10-10
### Added
- added 'spring-boot-starter-test' dependency on 'aba-boot-starter-test' pom

## [1.3.11] - 2016-10-06
### Added
- Added CustomReplacementDataSetLoader in aba-boot-data and a new ServerErrorApiException in aba-boot-exception.

## [1.3.9] - 2016-09-15
### Added
- Add an autoconfiguration class to change the log level of any request.

### Fixed
- When RabbitMQ is executing and amqp.dummy is set to true microservice must not try to connect to RabbitMQ, fixed an issue regarding to this behavior.

## [1.3.8] - 2016-09-14
### Added
- Add an autoconfiguration class to log all requests done to the microservice.

## [1.3.7] - 2016-09-08
### Added
- Added a timeout property to queue configuration. If amqp.queues[i].timeout is present, it will set x-message-ttl to the event headers.
- Added maven-javadoc-plugin to avoid errors when javadoc is incomplete.

## [1.3.6] - 2016-09-02
### Added
- Add new ApiExceptions: Conflict, Forbidden.
- DomainEventType interface now extends from Serializable.

## [1.3.5] - 2016-08-19
### Added
- Add amqp module to set a DomainEvent and an EventPublisher
- Add auto contiguration to set exchange, queues and bindings via application.yml
- Add testing configuration via DummyAmqpAutoConfiguration, adding amqp.dummy: true will "mock" an embedded Amqp application.

## [1.3.4] - 2016-08-05
### Add
- Add CodeMessage info to ApiException

## [1.3.3] - 2016-08-01
### Changed
- Change AbaDefaultErrorAttributes no show code when code is null

## [1.3.2] - 2016-07-28
### Changed
- Use of AbaDefaultErrorAttributes instead of DefaultErrorAttributes, adding code to error response.

## [1.3.1] - 2016-07-25
### Added
- Add new Bad Request Api exception.

## [1.3.0] - 2016-07-22
### Fixed
- Fix docker latest tag generation.

## [1.2.12] - 2016-07-20
### Changed
- New name policy for docker image tags:
    - develop -> ```x.y.z-SNAPSHOT-abbrev```
    - master -> ```x.y.z``` and ```latest```
    - bugfix and feature branches -> ```bugfix_????``` or ```feature_????```

## [1.2.11] - 2016-07-13
### Added
- Add message attribute to ApiException.

## [1.2.10] - 2016-07-12
### Fixed
- Remove orika-tests because was causing logging issues in microservices.

## [1.2.9] - 2016-07-06
### Added
- Add orika-tests to allow orika testing.

## [1.2.8] - 2016-07-06
### Changed
- Move build-helper-maven-plugin from aba-boot-starter-parent to parent pom.

## [1.2.6] - 2016-07-05
### Added
- Add querydsl-jpa dependency to allow querydsl creation.
- Add src/generated-sources/java as source folder for maven and IDE's

## [1.2.5] - 2016-07-04
### Added
- Add GoneApiException.

## [1.2.4] - 2016-07-04
### Added
- Add aba-boot-api with GlobalExceptionHandler as @ControllerAdvice.
- Add aba-boot-exception to set common aba exceptions.

## [1.2.3] - 2016-06-30
### Added
- Add spring-cloud-config dependency to add config-server connection in all microservices.

### Changed
- Update spring-cloud-netflix version from 1.1.0.RELEASE to 1.1.2.RELEASE.

## [1.2.2] - 2016-06-29
### Added
- Add aba-boot-domain to set abstract class for domain objects.

## [1.2.1] - 2016-06-29
### Added
- Add dbunit and spring-test-db-unit dependencies to test database repositories.
- Add HikariCP as connection pool.

## [1.2.0] - 2016-06-28
### Added
- Add aba-boot-data to set a BaseRepository.

## [1.1.9] - 2016-06-28
### Added
- Add jackson-datatype-jsr310 dependency to make jackson recognize Java 8 Date & Time API data types.

## [1.1.6] - 2016-06-20
### Added
- Add aba-boot-http-request to set BasicAuthInterceptor to RestTemplate.

## [1.1.5] - 2016-06-16
### Added
- Add springfox dependency for swagger.
- Add docker registry url to docker-maven-plugin.

### Changed
- Use of spring-mvc instead of jersey.

## [1.1.4] - 2016-06-13
### Added
- deleteDir() command to all Jenkinsfile to avoid issues with previous builds in jenkins.
- Docker plugin to allow docker image management.

### Fixed
- Integration tests resources in src/integration-test/resoruces are now added with integration test classes.

## [1.1.3] - 2016-06-10
### Added
- aba-boot-starter-test module to allow test creation in any project.
- rest-assured as test library.

### Changed
- Integration tests has been separeted from unit tests in src/integration-test.
- There are three Jenkinsfile now, Jenkinsfile for pull requests, Jenkinsfile.develop for CI, Jenkinsfile for releases.

## [1.1.2] - 2016-06-10
### Added
- squash option to true in jgitflow-maven-plugin.

### Fixed
- Fix jersey issue when jar is executed.

## [1.1.1] - 2016-06-10
### Added
- spring-boot-configuration-processor to show custom autocomplete in yml.
- Tests for autoconfiguration: BuildProperties and JerseyResourceAutoConfiguration.

## [1.1.0] - 2016-06-09
### Added
- Orika implementation and autoconfiguration.

### Fixed
- autoVersionSubmodules set to true to avoid release issues.

## 1.0.0 - 2016-06-07
### Added
- Initial project structure.
- aba-boot-starter-parent for aba projects.
- aba-boot-starter-api module for api projects.
- aba-boot-starter-data module for data projects.
- Autoconfiguration for CORS.
- Autoconfiguration for Jersey and Swagger.
- jgitflow-maven-plugin to enable git-flow style releases via maven.

[Unreleased]: https://github.com/abaenglish/aba-boot/compare/v1.3.14...HEAD
[1.3.14]: https://github.com/abaenglish/aba-boot/compare/v1.3.13...v1.3.14
[1.3.13]: https://github.com/abaenglish/aba-boot/compare/v1.3.12...v1.3.13
[1.3.12]: https://github.com/abaenglish/aba-boot/compare/v1.3.11...v1.3.12
[1.3.11]: https://github.com/abaenglish/aba-boot/compare/v1.3.10...v1.3.11
[1.3.10]: https://github.com/abaenglish/aba-boot/compare/v1.3.9...v1.3.10
[1.3.9]: https://github.com/abaenglish/aba-boot/compare/v1.3.8...v1.3.9
[1.3.8]: https://github.com/abaenglish/aba-boot/compare/v1.3.7...v1.3.8
[1.3.7]: https://github.com/abaenglish/aba-boot/compare/v1.3.6...v1.3.7
[1.3.6]: https://github.com/abaenglish/aba-boot/compare/v1.3.5...v1.3.6
[1.3.5]: https://github.com/abaenglish/aba-boot/compare/v1.3.4...v1.3.5
[1.3.4]: https://github.com/abaenglish/aba-boot/compare/v1.3.3...v1.3.4
[1.3.3]: https://github.com/abaenglish/aba-boot/compare/v1.3.2...v1.3.3
[1.3.2]: https://github.com/abaenglish/aba-boot/compare/v1.3.1...v1.3.2
[1.3.1]: https://github.com/abaenglish/aba-boot/compare/v1.3.0...v1.3.1
[1.3.0]: https://github.com/abaenglish/aba-boot/compare/v1.2.12...v1.3.0
[1.2.12]: https://github.com/abaenglish/aba-boot/compare/v1.2.11...v1.2.12
[1.2.11]: https://github.com/abaenglish/aba-boot/compare/v1.2.10...v1.2.11
[1.2.10]: https://github.com/abaenglish/aba-boot/compare/v1.2.9...v1.2.10
[1.2.9]: https://github.com/abaenglish/aba-boot/compare/v1.2.8...v1.2.9
[1.2.8]: https://github.com/abaenglish/aba-boot/compare/v1.2.7...v1.2.8
[1.2.6]: https://github.com/abaenglish/aba-boot/compare/v1.2.5...v1.2.6
[1.2.5]: https://github.com/abaenglish/aba-boot/compare/v1.2.4...v1.2.5
[1.2.4]: https://github.com/abaenglish/aba-boot/compare/v1.2.3...v1.2.4
[1.2.3]: https://github.com/abaenglish/aba-boot/compare/v1.2.2...v1.2.3
[1.2.2]: https://github.com/abaenglish/aba-boot/compare/v1.2.1...v1.2.2
[1.2.1]: https://github.com/abaenglish/aba-boot/compare/v1.2.0...v1.2.1
[1.2.0]: https://github.com/abaenglish/aba-boot/compare/v1.1.9...v1.2.0
[1.1.9]: https://github.com/abaenglish/aba-boot/compare/v1.1.8...v1.1.9
[1.1.6]: https://github.com/abaenglish/aba-boot/compare/v1.1.5...v1.1.6
[1.1.5]: https://github.com/abaenglish/aba-boot/compare/v1.1.4...v1.1.5
[1.1.4]: https://github.com/abaenglish/aba-boot/compare/v1.1.3...v1.1.4
[1.1.3]: https://github.com/abaenglish/aba-boot/compare/v1.1.2...v1.1.3
[1.1.2]: https://github.com/abaenglish/aba-boot/compare/v1.1.1...v1.1.2
[1.1.1]: https://github.com/abaenglish/aba-boot/compare/v1.1.0...v1.1.1
[1.1.0]: https://github.com/abaenglish/aba-boot/compare/v1.0.0...v1.1.0
