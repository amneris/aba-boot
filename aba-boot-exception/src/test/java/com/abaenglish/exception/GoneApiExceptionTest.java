package com.abaenglish.exception;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.GoneApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class GoneApiExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGoneException() throws GoneApiException {

        thrown.expect(GoneApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.GONE)));

        throw new GoneApiException(new Exception());
    }

    @Test
    public void testGone() throws GoneApiException {

        thrown.expect(GoneApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.GONE)));

        throw new GoneApiException();
    }

    @Test
    public void testGoneMessage() throws GoneApiException {

        thrown.expect(GoneApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.GONE)));
        thrown.expect(hasProperty("message", is("NOT FOUND")));

        throw new GoneApiException("NOT FOUND");
    }

    @Test
    public void testGoneCodeMessageObject() throws GoneApiException {

        CodeMessage error = new CodeMessage("ERROR", "ERROR FOUND");

        thrown.expect(GoneApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.GONE)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new GoneApiException(error);
    }

    @Test
    public void testGoneCodeWithServiceException() throws GoneApiException {

        CodeMessage error = new CodeMessage("ERROR", "ERROR FOUND");

        thrown.expect(GoneApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.GONE)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new GoneApiException(new ServiceException(error));
    }

}
