package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class NotFoundApiException extends ApiException {

    private static final long serialVersionUID = 1678908311043357150L;

    public NotFoundApiException(Exception ex) {
        super(ex, HttpStatus.NOT_FOUND);
    }

    public NotFoundApiException() {
        super(HttpStatus.NOT_FOUND);
    }

    public NotFoundApiException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }

    public NotFoundApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.NOT_FOUND);
    }

    public NotFoundApiException(ServiceException ex) {
        super(ex, HttpStatus.NOT_FOUND);
    }

}
