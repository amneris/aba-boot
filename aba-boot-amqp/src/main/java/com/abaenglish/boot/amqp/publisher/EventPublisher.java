package com.abaenglish.boot.amqp.publisher;

import com.abaenglish.boot.amqp.domain.DomainEvent;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Helper class that simplifies the publication of events in RabbitMQ
 */
public abstract class EventPublisher {

    private static final Logger logger = LoggerFactory.getLogger(EventPublisher.class);
    public static final String MESSAGE_HEADER_TIMESTAMP = "aba-message-timestamp";
    public static final String MESSAGE_HEADER_EXCEPTION_MESSAGE = "aba-exception-message";
    public static final String MESSAGE_HEADER_EXCEPTION_CAUSE = "aba-exception-cause";
    public static final String MESSAGE_HEADER_EXCEPTION_CODE = "aba-exception-code";

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * Sends an event to the exchange
     * @param event the event sent to the exchange
     */
    protected void sendEvent(final DomainEvent event) {
        sendEvent(event, new HashMap<>());
    }


    /**
     * Sends an event as an error and the exception related to the exchange
     *
     * @param event
     * @param e
     */
    protected void sendError(final DomainEvent event, final Throwable e) {
        sendError(event, Optional.empty(), Optional.of(e));
    }

    /**
     * Sends an event as an error and the exception related to the exchange and errorCode related
     *
     * @param event
     * @param errorCode
     * @param e
     */
    protected void sendError(final DomainEvent event, final Optional<String> errorCode, final Optional<Throwable> e) {

        final Map<String, String> headers = new HashMap<>();
        e.ifPresent( error -> {
            headers.put(MESSAGE_HEADER_EXCEPTION_MESSAGE, error.getMessage());
            headers.put(MESSAGE_HEADER_EXCEPTION_CAUSE, ExceptionUtils.getRootCauseMessage(error));
        });
        errorCode.ifPresent( code -> headers.put(MESSAGE_HEADER_EXCEPTION_CODE, code) );
        sendEvent(event, headers);
    }

    /**
     * Put default parameters into map of headers provided
     * Default parameters:
     *  - MESSAGE_HEADER_TIMESTAMP: current time before the message is send
     *
     * @param headers
     */
    private void buildDefaultHeaders(final Map<String, String> headers) {
        headers.put(MESSAGE_HEADER_TIMESTAMP, LocalDateTime.now().toString());
    }

    /**
     * Sends an event with headers to the exchange.
     * Default headers are added to provided headers
     * @see EventPublisher#buildDefaultHeaders(Map)
     *
     * @param event the event sent to the exchange
     * @param headers headers of the event
     */
    protected void sendEvent(DomainEvent event, Map<String, String> headers) {
        buildDefaultHeaders(headers);

        MessageProperties messageProperties = new MessageProperties();

        if (headers != null) {
            headers.forEach(messageProperties::setHeader);
        }

        Message message = rabbitTemplate.getMessageConverter().toMessage(event, messageProperties);

        rabbitTemplate.send(event.getExchange(), event.getRoutingKey(), message);
        logger.info("event type <{}> sended to exchange <{}> with routing key <{}> and headers <{}>", event.getEventType(), event.getExchange(), event.getRoutingKey(), headers.toString());
    }

}
