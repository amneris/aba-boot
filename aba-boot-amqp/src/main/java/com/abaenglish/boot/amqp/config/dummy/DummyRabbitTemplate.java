package com.abaenglish.boot.amqp.config.dummy;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public class DummyRabbitTemplate extends RabbitTemplate {

    public DummyRabbitTemplate(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    @Override
    public void convertAndSend(String exchange, String routingKey, Object object) throws AmqpException {
        // This method must send object to DummyRabbitAdmin dummy queue.
    }

}
