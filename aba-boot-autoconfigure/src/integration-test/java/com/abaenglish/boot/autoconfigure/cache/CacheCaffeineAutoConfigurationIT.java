package com.abaenglish.boot.autoconfigure.cache;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class CacheCaffeineAutoConfigurationIT {

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = {CacheCaffeineAutoConfiguration.class, CacheAutoConfiguration.class}, properties = {"spring.cache.enabled = true"}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
    public static class PropertyTrueIT {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void propertyTrue() {
            assertThat(context.getBeanNamesForType(CacheCaffeineAutoConfiguration.class).length).isEqualTo(1);
            assertThat(context.getBeanNamesForType(CacheAutoConfiguration.class).length).isEqualTo(1);
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = {CacheCaffeineAutoConfiguration.class, CacheAutoConfiguration.class}, properties = {"spring.cache.enabled = false"}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
    public static class PropertyFalseIT {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void propertyTrue() {
            assertThat(context.getBeanNamesForType(CacheCaffeineAutoConfiguration.class).length).isEqualTo(0);
            assertThat(context.getBeanNamesForType(CacheAutoConfiguration.class).length).isEqualTo(0);
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = {CacheCaffeineAutoConfiguration.class, CacheAutoConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
    public static class PropertyNotExistsIT {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void propertyTrue() {
            assertThat(context.getBeanNamesForType(CacheCaffeineAutoConfiguration.class).length).isEqualTo(0);
            assertThat(context.getBeanNamesForType(CacheAutoConfiguration.class).length).isEqualTo(0);
        }
    }

}
