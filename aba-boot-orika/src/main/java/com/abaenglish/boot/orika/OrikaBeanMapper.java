package com.abaenglish.boot.orika;

import ma.glasnost.orika.Converter;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class OrikaBeanMapper extends ConfigurableMapper implements ApplicationContextAware {

    private MapperFactory factory;
    private ApplicationContext applicationContext;

    /**
     * In case you need to instance OrikaBeanMapper as a UnitTest variable you can do it by autoInit = true
     */
    public OrikaBeanMapper(boolean autoInit) {
        super(autoInit);
    }

    public OrikaBeanMapper() {
        super(false);
    }

    @Override
    protected void configure(MapperFactory factory) {
        this.factory = factory;
        addAllSpringBeans(applicationContext);
    }

    @Override
    protected void configureFactoryBuilder(final DefaultMapperFactory.Builder factoryBuilder) {
        // Nothing to do
    }

    @SuppressWarnings("rawtypes")
    private void addAllSpringBeans(final ApplicationContext applicationContext) {
        if (applicationContext != null) {
            applicationContext.getBeansOfType(Mapper.class).values().forEach(this::addMapper);
            applicationContext.getBeansOfType(Converter.class).values().forEach(this::addConverter);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public void addMapper(Mapper<?, ?> mapper) {
        ClassMapBuilder<?, ?> classMap = factory.classMap(mapper.getAType(), mapper.getBType())
                .customize((Mapper) mapper);

        if (mapper instanceof BaseCustomMapper) {
            BaseCustomMapper baseCustomMapper = (BaseCustomMapper) mapper;
            if (!baseCustomMapper.getExcludes().isEmpty()) {
                baseCustomMapper.getExcludes().forEach(field -> classMap.exclude((String) field));
            }

            if (!baseCustomMapper.getFields().isEmpty()) {
                baseCustomMapper.getFields().forEach((k, v) -> classMap.field((String) k, (String) v));
            }
        }

        classMap.byDefault().register();
    }

    public void addConverter(Converter<?, ?> converter) {
        factory.getConverterFactory().registerConverter(converter);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        init();
    }

}

