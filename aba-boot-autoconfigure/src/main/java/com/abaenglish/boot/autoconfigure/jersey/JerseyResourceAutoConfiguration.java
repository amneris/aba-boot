package com.abaenglish.boot.autoconfigure.jersey;

import com.abaenglish.boot.autoconfigure.properties.BuildProperties;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration;
import org.springframework.boot.autoconfigure.jersey.JerseyProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({ ResourceConfig.class })
@AutoConfigureBefore(value = {JerseyAutoConfiguration.class})
@EnableConfigurationProperties({ BuildProperties.class, JerseyProperties.class })
public class JerseyResourceAutoConfiguration {

    @Autowired
    private BuildProperties build;

    @Autowired
    private JerseyProperties jersey;

    @Bean
    public ResourceAutoConfiguration config() {
        return new ResourceAutoConfiguration();
    }

    private class ResourceAutoConfiguration extends ResourceConfig {

        public ResourceAutoConfiguration() {
            this.configureEndpoints();
            this.configureSwagger();
        }

        private void configureEndpoints() {
            packages("com.abaenglish");
            register(WadlResource.class);
        }

        private void configureSwagger() {
            this.register(ApiListingResource.class);
            this.register(SwaggerSerializers.class);

            BeanConfig config = new BeanConfig();
            config.setConfigId(build.getArtifact());
            config.setTitle(build.getName());
            config.setDescription(build.getDescription());
            config.setVersion(build.getVersion());
            config.setSchemes(new String[]{"http"});
            config.setBasePath(jersey.getApplicationPath());
            config.setResourcePackage("com.abaenglish");
            config.setPrettyPrint(true);
            config.setScan(true);
        }

    }

}
