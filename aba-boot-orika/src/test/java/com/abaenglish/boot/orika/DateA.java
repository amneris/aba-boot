package com.abaenglish.boot.orika;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateA {

    public DateA(LocalDate localDate, LocalDateTime localDateTime) {
        this.localDate = localDate;
        this.localDateTime = localDateTime;
    }

    public LocalDate localDate;
    public LocalDateTime localDateTime;
}
