package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class ServerErrorApiException extends ApiException {

    private static final long serialVersionUID = 8099146305484334153L;

    public ServerErrorApiException(Exception ex) {
        super(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ServerErrorApiException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ServerErrorApiException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ServerErrorApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ServerErrorApiException(ServiceException ex) {
        super(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
