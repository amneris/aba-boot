package com.abaenglish.boot.autoconfigure.springfox;

import com.abaenglish.boot.autoconfigure.properties.BuildProperties;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import springfox.documentation.spring.web.plugins.Docket;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class SpringfoxAutoConfigurationTest {

    @Mock
    private BuildProperties properties;

    @InjectMocks
    private SpringfoxAutoConfiguration configuration;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void jerseyResourceAutoConfiguration() {
        when(properties.getName()).thenReturn("name-test");
        when(properties.getDescription()).thenReturn("description-test");
        when(properties.getVersion()).thenReturn("1.0.-SNAPSHOT");

        Docket docket = configuration.api();
        assertThat(docket).isNotNull();
    }

}
