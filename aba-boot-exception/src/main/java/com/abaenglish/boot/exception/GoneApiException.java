package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class GoneApiException extends ApiException {

    private static final long serialVersionUID = -4938284262782573665L;

    public GoneApiException(Exception ex) {
        super(ex, HttpStatus.GONE);
    }

    public GoneApiException() {
        super(HttpStatus.GONE);
    }

    public GoneApiException(String message) {
        super(message, HttpStatus.GONE);
    }

    public GoneApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.GONE);
    }

    public GoneApiException(ServiceException ex) {
        super(ex, HttpStatus.GONE);
    }

}
