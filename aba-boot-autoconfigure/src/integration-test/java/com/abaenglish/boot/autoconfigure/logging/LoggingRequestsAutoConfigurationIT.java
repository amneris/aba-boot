package com.abaenglish.boot.autoconfigure.logging;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class LoggingRequestsAutoConfigurationIT {

    @RunWith(SpringJUnit4ClassRunner.class)
    @SpringApplicationConfiguration(classes={ LoggingRequestsAutoConfiguration.class })
    @IntegrationTest
    public static class WithoutProperty {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void withoutProperty() {
            assertThat(context.getBeanNamesForType(CommonsRequestLoggingFilter.class).length).isEqualTo(0);
        }

    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @SpringApplicationConfiguration(classes={ LoggingRequestsAutoConfiguration.class })
    @TestPropertySource(properties = { "logging.level.org.springframework.web.filter.CommonsRequestLoggingFilter = DEBUG" })
    @IntegrationTest
    public static class WithProperty {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void withProperty() {
            assertThat(context.getBeanNamesForType(CommonsRequestLoggingFilter.class).length).isEqualTo(1);
        }

    }

}
