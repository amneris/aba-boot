package com.abaenglish.exception;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.ConflictApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class ConflictApiExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testConflictException() throws ConflictApiException {

        thrown.expect(ConflictApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.CONFLICT)));

        throw new ConflictApiException(new Exception());
    }

    @Test
    public void testConflict() throws ConflictApiException {

        thrown.expect(ConflictApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.CONFLICT)));

        throw new ConflictApiException();
    }

    @Test
    public void testConflictMessage() throws ConflictApiException {

        thrown.expect(ConflictApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.CONFLICT)));
        thrown.expect(hasProperty("message", is("CONFLICT")));

        throw new ConflictApiException("CONFLICT");
    }
    
    @Test
    public void testConflictCodeMessageObject() throws ConflictApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR CONFLICT");

        thrown.expect(ConflictApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.CONFLICT)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new ConflictApiException(error);
    }

    @Test
    public void testConflictWithServiceException() throws ConflictApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR CONFLICT");

        thrown.expect(ConflictApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.CONFLICT)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new ConflictApiException(new ServiceException(error));
    }

}
