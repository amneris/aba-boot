package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class BadRequestApiException extends ApiException {

    private static final long serialVersionUID = 2282533612912715846L;

    public BadRequestApiException(Exception ex) {
        super(ex, HttpStatus.BAD_REQUEST);
    }

    public BadRequestApiException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public BadRequestApiException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }

    public BadRequestApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.BAD_REQUEST);
    }

    public BadRequestApiException(ServiceException ex) {
        super(ex, HttpStatus.BAD_REQUEST);
    }

}
