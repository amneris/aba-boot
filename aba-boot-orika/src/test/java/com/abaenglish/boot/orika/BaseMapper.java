package com.abaenglish.boot.orika;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;

public class BaseMapper extends CustomMapper<Person, PersonResponse> {

    @Override
    public void mapAtoB(Person person, PersonResponse personResponse, MappingContext context) {
        super.mapAtoB(person, personResponse, context);
    }

}
