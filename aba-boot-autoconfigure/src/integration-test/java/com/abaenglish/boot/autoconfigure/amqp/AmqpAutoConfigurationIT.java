package com.abaenglish.boot.autoconfigure.amqp;

import com.abaenglish.boot.amqp.config.dummy.DummyRabbitAdmin;
import com.abaenglish.boot.amqp.config.dummy.DummyRabbitTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.connection.SimpleRoutingConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DummyAmqpAutoConfiguration.class, AmqpAdminAutoConfiguration.class, AmqpAutoConfiguration.class, RabbitAutoConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestPropertySource("/application-amqp.yml")
public class AmqpAutoConfigurationIT {

    @Autowired
    AnnotationConfigApplicationContext context;

    @Autowired
    AmqpProperties amqpProperties;

    @Test
    public void amqpAutoConfiguration() {
        assertThat(context.getBeanNamesForType(SimpleRoutingConnectionFactory.class).length).isEqualTo(1);
        assertThat(context.getBeanNamesForType(DummyRabbitAdmin.class).length).isEqualTo(1);
        assertThat(context.getBeanNamesForType(DummyRabbitTemplate.class).length).isEqualTo(1);
    }

    @Test
    public void canBindAllProperties() {
        assertThat(amqpProperties.getExchange().get()).isEqualTo("test.exchange");
        assertThat(amqpProperties.getDummy()).isEqualTo(true);
        assertThat(amqpProperties.getQueues().size()).isEqualTo(2);
        assertThat(amqpProperties.getQueues().get(0).getExchange()).isEqualTo("test.exchange");
        assertThat(amqpProperties.getQueues().get(0).getName()).isEqualTo("test.queue1.name");
        assertThat(amqpProperties.getQueues().get(0).getRoutingKeys().get(0)).isEqualTo("[\"*\"]");
        assertThat(amqpProperties.getQueues().get(1).getExchange()).isEqualTo("test.exchange");
        assertThat(amqpProperties.getQueues().get(1).getName()).isEqualTo("test.queue2.name");
        assertThat(amqpProperties.getQueues().get(1).getRoutingKeys().get(0)).isEqualTo("[\"*\"]");
        assertThat(amqpProperties.getQueues().get(1).getTimeout()).isEqualTo(5000);
    }

}
