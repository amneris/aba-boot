package com.abaenglish.boot.amqp.domain;

import java.io.Serializable;

public interface DomainEventType extends Serializable {

    String getExchange();

    String getRoutingKey();

}
