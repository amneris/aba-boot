# Aba Boot

[![Build Status](http://jenkins.aba.land:8080/buildStatus/icon?job=Backend/Aba Boot/branches/develop)](http://jenkins.aba.land:8080/job/Backend/job/Aba%20Boot/job/branches/job/develop/)
[![Quality Gate](https://sonar.aba.land/api/badges/gate?key=com.abaenglish.backend.boot%3Aaba-boot-build%3Adevelop)](https://sonar.aba.land/overview?id=com.abaenglish.backend.boot%3Aaba-boot-build%3Adevelop)
[![Coverage](https://sonar.aba.land/api/badges/measure?key=com.abaenglish.backend.boot%3Aaba-boot-build%3Adevelop&metric=overall_coverage)](https://sonar.aba.land/overview?id=com.abaenglish.backend.boot%3Aaba-boot-build%3Adevelop)

This is a [Microservice chassis framework](http://microservices.io/patterns/microservice-chassis.html) based on [Spring Boot](https://github.com/spring-projects/spring-boot)

Primary goals:
- Provide a faster and widely accessible getting started experience for all Aba microservices.
- Avoid unnecesary and duplicated configuration

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Usage](#usage)
- [Log Management](#log-management)
  - [Centralized log system](#centralized-log-system)
  - [Logging all requests](#logging-all-requests)
  - [Set log level of a single request](#set-log-level-of-a-single-request)
- [Message Broker (Event Bus)](#message-broker-event-bus)
  - [Developing with Amqp](#developing-with-amqp)
  - [Most important classes and annotations](#most-important-classes-and-annotations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Usage

All microservices have to use aba-boot-starter-parent as parent.

These are the starters availables to import on microservice projects:
- aba-boot-starter-api: A wrapper for spring-boot-starter-web with some extra dependencies:
  - [jetty](http://www.eclipse.org/jetty/): Provides a Web server and javax.servlet container.
  - [actuator](http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready): Provides additional features to monitor and manage applications.
  - [springfox-swagger](http://springfox.github.io/springfox/): Provides automated JSON API documentation for swagger.
- aba-boot-starter-data: A wrapper for spring-boot-starter-data-jpa with some extra dependencies:
  - [flyway](https://flywaydb.org/): Provides database migration.
  - [HikariCP](https://brettwooldridge.github.io/HikariCP/): Provides a connection pool.
  - [querydsl-jpa](http://www.querydsl.com/index.html): Provides querydsl support for Spring Data JPA.
  - mysql: As default database connector.
  - h2: As database for testing purposes.
- aba-boot-starter-test: A wrapper for spring-boot-starter-test with some extra dependencies:
  - [rest-assured](https://github.com/rest-assured/rest-assured): Provides testing and validation of REST services.
  - [spring-test-db-unit](https://springtestdbunit.github.io/spring-test-dbunit/): Provides database testing as an integration between the Spring testing framework and DBUnit.
  - [assertj](http://joel-costigliola.github.io/assertj/): Provides assertions for all testing.
- aba-boot-starter-amqp: A wrapper for spring-boot-starter-amqp

## Log Management

### Centralized log system

Log management is ruled by logback autoconfiguration. We will use a cloud-based service to manage all production logs. To be able to use those services we will need to set the log output format to json.

Adding these lines to our application.yml (via classpath file or [config server file](https://github.com/abaenglish/cloud-config-server-properties)) we will enable the json output format: 
```yaml
logging:
  config: ${spring.cloud.config.uri}/subscription-service/default/master/logback-spring.xml
```

It is recommended to use for production environments only.

### Logging all requests

Adding these lines to the application.yml of a microservice we will log all requests done to the microservice.

```yaml
logging:
  level.org.springframework.web.filter.CommonsRequestLoggingFilter: DEBUG
```

### Set log level of a single request

All aba-boot microservices can set the log level to TRACE of a single request adding ```trace=on``` as a param of the querystring.

If any microservice want to disable this behavior you can add the following line to the application.yml:
```yaml
logging.dynamicLogLevel: disable
```

## Message Broker (Event Bus)

Aba-boot supports Amqp since 1.3.5

### Developing with Amqp

You can obtain amqp support adding aba-boot-starter-amqp to your pom.xml.

If we want to create an exchange and some queues you will need to add the following lines to the application.yml:

```yaml
spring:
  rabbitmq:
    port: 5672                          # RabbitMQ default port

amqp:
  exchange: your-service                # Creates an exchange
  queues:
    -
      name: your-service.created        # Creates a queue with the given name
      exchange: subscription-service    # binded to this exchange
      routingKeys: ["CREATED"]          # and binded to this routingKey
    -
      name: your-service.removed
      exchange: subscription-service
      routingKeys: ["REMOVED"]
```

If you want to try amqp implementation without RabbitMQ running on your system you can add ```amqp.dummy: true``` to your application.yml to avoid RabbitMQ connection. It can be more useful when you want to test your application.

### Most important classes and annotations

Aba-boot provide some classes to make easy the amqp implementation.

* DomainEvent: All bus events must inherit from this class.

* DomainEventType: All bus events must have a type. These types will be defined by an enum that implements DomainEventType.

* EventPublisher: All publishers must inherit from this class. EventPublisher implements a method to send the event to the exchange based in the DomainEventType

* @RabbitListener: This annotation allows to be listening messages from a queue.
