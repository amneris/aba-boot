package com.abaenglish.boot.orika;

import org.springframework.stereotype.Component;

@Component
public class PersonToPersonResponseFieldMapper extends BaseCustomMapper<Person, PersonResponse> {

    public PersonToPersonResponseFieldMapper() {
        super();

        addField("name", "firstName");
    }

}
