#!/usr/bin/env groovy

stage('build') {
    node {
        deleteDir()
        checkout scm
        withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
            sh "mvn -B clean verify -DskipTests"
        }

        stash excludes: 'target/', name: 'source'
    }
}

stage('tests') {
    node {
        unstash 'source'

        withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
            sh "mvn -B verify -Pcoverage"
        }

        step([$class: 'JUnitResultArchiver', testResults: '**/target/*-reports/*.xml'])
    }
}

if ("${env.BRANCH_NAME}".startsWith('PR-')) {
    // This is a pull request.
    stage('code quality') {
        node {
            unstash 'source'

            def pullRequest = "${env.BRANCH_NAME}".replace('PR-', '')

            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'sonarqube', passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN']]) {
                withCredentials([[$class: 'StringBinding', credentialsId: '7cacb33c-5bfc-4d39-b678-a921023123d9', variable: 'GITHUB_ACCESS_TOKEN']]) {
                    withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                        sh "mvn -e -B sonar:sonar -Dsonar.host.url=http://sonar.aba.land:9000/ " +
                                "-Dsonar.branch=develop " +
                                "-Dsonar.login=${env.SONAR_LOGIN} " +
                                "-Dsonar.password=${env.SONAR_PASSWORD} " +
                                "-Dsonar.analysis.mode=preview " +
                                "-Dsonar.github.pullRequest=${pullRequest} " +
                                "-Dsonar.github.repository=abaenglish/aba-boot " +
                                "-Dsonar.github.oauth=${env.GITHUB_ACCESS_TOKEN}"
                    }
                }
            }
        }
    }
}
else if ("${env.BRANCH_NAME}".startsWith('develop')) {
    stage('code quality') {
        node {
            unstash 'source'

            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'sonarqube', passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN']]) {
                withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                    sh "mvn -e -B sonar:sonar -Dsonar.host.url=http://sonar.aba.land:9000/ -Dsonar.branch=develop -Dsonar.login=${env.SONAR_LOGIN} -Dsonar.password=${env.SONAR_PASSWORD}"
                }
            }
        }
    }

    stage('publish artifact') {
        node {
            unstash 'source'

            withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                sh "mvn -B deploy -DskipTests"
            }
        }
    }
}
