package com.abaenglish.boot.autoconfigure.amqp;

import com.abaenglish.boot.exception.service.queue.RetryableServiceException;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties(AmqpProperties.class)
@ConditionalOnClass({RabbitTemplate.class, Channel.class})
public class AmqpAutoConfiguration {

    @Autowired
    AmqpProperties amqpProperties;

    @Bean
    @ConditionalOnMissingBean(RabbitTemplate.class)
    @ConditionalOnProperty(prefix = "amqp", name = "exchange")
    RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    /**
     * Primary generic bean RabbitListenerContainerFactory for <b>not retryable</b> queues with this configuration:
     * <ul>
     * <li> No retryable, all errors send to deadletter queue
     * <li> Message converter: {@link Jackson2JsonMessageConverter}
     * <p>
     * Only created when {@code amqpProperties.queues[0].name} exists, {@link AmqpProperties}
     *
     * @param rabbitConnectionFactory
     * @return
     */
    @Bean
    @Primary
    @ConditionalOnProperty(prefix = "amqp", name = "queues[0].name")
    RabbitListenerContainerFactory<SimpleMessageListenerContainer> rabbitListenerContainerFactory(ConnectionFactory rabbitConnectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(rabbitConnectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        factory.setDefaultRequeueRejected(false);
        return factory;
    }

    /**
     * Generic bean RabbitListenerContainerFactory for <b>retryable</b> queues with this configuration:
     * <ul>
     * <li> Retry all errors that extends {@link RetryableServiceException}
     * <li> Message converter: {@link Jackson2JsonMessageConverter}
     * <li> Retry configuration loaded from properties in {@link AmqpProperties#getRetry()}
     * <p>
     * Only created when {@code amqpProperties.retry.maxAttempts} exists, {@link AmqpProperties}
     *
     * @param rabbitConnectionFactory
     * @return
     */
    @Bean(name = "rabbitRetryListenerContainerFactory")
    @ConditionalOnMissingBean(name = "rabbitRetryListenerContainerFactory")
    @ConditionalOnProperty(prefix = "amqp", name = "retry.maxAttempts")
    RabbitListenerContainerFactory<SimpleMessageListenerContainer> rabbitRetryListenerContainerFactory(ConnectionFactory rabbitConnectionFactory) {
        final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(rabbitConnectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        factory.setDefaultRequeueRejected(true);


        final Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap();
        retryableExceptions.put(RetryableServiceException.class, true);

        final SimpleRetryPolicy policy = new SimpleRetryPolicy(amqpProperties.getRetry().getMaxAttempts(), retryableExceptions, true);

        final ExponentialBackOffPolicy backOff = new ExponentialBackOffPolicy();
        backOff.setInitialInterval(amqpProperties.getRetry().getBackOff().getInitialInterval());
        backOff.setMultiplier(amqpProperties.getRetry().getBackOff().getMultiplier());
        backOff.setMaxInterval(amqpProperties.getRetry().getBackOff().getMaxInterval());

        final RetryInterceptorBuilder builder = RetryInterceptorBuilder.stateless()
                .retryPolicy(policy)
                .backOffPolicy(backOff)
                .recoverer(new RejectAndDontRequeueRecoverer());
        factory.setAdviceChain(builder.build());
        return factory;
    }

}

