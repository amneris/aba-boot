package com.abaenglish.boot.exception.service.queue;

import com.abaenglish.boot.exception.service.ServiceException;

/**
 * Thrown to indicate that this error is temporary and could be retried the process that launched it.
 * This is a managed exception by us , then it extends {@link ServiceException}
 * The retryable is available only in a Rabbit consumer context.
 */
public class RetryableServiceException extends ServiceException {

    private static final long serialVersionUID = -2339329934255999540L;

    public RetryableServiceException(Throwable cause) {
        super("Retryable error", cause);
    }

    public RetryableServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
