package com.abaenglish.boot.autoconfigure.logging;

import ch.qos.logback.classic.Level;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(Level.class)
@ConditionalOnProperty(value = "logging.dynamicLogLevel", havingValue = "enable", matchIfMissing = true)
public class DynamicLogLevelAutoConfiguration {

    @Bean
    ThreadLoggingFilterBean threadLoggingFilterBean() {
        return new ThreadLoggingFilterBean();
    }

    @Bean
    ThreadLoggingInitializer threadLoggingInitializer() {
        return new ThreadLoggingInitializer();
    }

}
