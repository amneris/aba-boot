package com.abaenglish.boot.amqp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class DomainEvent implements Serializable {

    private static final long serialVersionUID = -8072701852318598122L;

    private final String eventType;

    @JsonIgnore
    private final DomainEventType domainEventType;

    public DomainEvent(DomainEventType type) {
        this.domainEventType = type;
        this.eventType = type.toString();
    }

    public String getEventType() {
        return eventType;
    }

    public String getRoutingKey() {
        return domainEventType.getRoutingKey();
    }

    public String getExchange() {
        return domainEventType.getExchange();
    }

}
