package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public abstract class ApiException extends Exception {

    private static final long serialVersionUID = 4958689719277571957L;

    private final HttpStatus httpStatus;
    private final String apiMessage;
    private final String code;

    public ApiException(HttpStatus httpStatus) {
        super();
        this.httpStatus = httpStatus;
        this.apiMessage = null;
        this.code = null;
    }

    public ApiException(String apiMessage, HttpStatus httpStatus) {
        super(apiMessage);
        this.httpStatus = httpStatus;
        this.apiMessage = apiMessage;
        this.code = null;
    }

    public ApiException(Exception ex, final HttpStatus status) {
        super(ex);
        this.httpStatus = status;
        this.apiMessage = ex.getMessage();
        this.code = null;
    }

    public ApiException(CodeMessage codeMessage, HttpStatus httpStatus) {
        super(codeMessage.getMessage());
        this.httpStatus = httpStatus;
        this.code = codeMessage.getCode();
        this.apiMessage = codeMessage.getMessage();
    }

    public ApiException(ServiceException ex, HttpStatus httpStatus) {
        super(ex.getCodeMessage().getMessage());
        this.httpStatus = httpStatus;
        this.code = ex.getCodeMessage().getCode();
        this.apiMessage = ex.getCodeMessage().getMessage();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getApiMessage() {
        return apiMessage;
    }

    public String getCode() {
        return code;
    }

}
