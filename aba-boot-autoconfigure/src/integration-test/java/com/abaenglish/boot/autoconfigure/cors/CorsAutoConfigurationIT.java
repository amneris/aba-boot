package com.abaenglish.boot.autoconfigure.cors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes=CorsAutoConfiguration.class)
public class CorsAutoConfigurationIT {

    @Autowired
    ApplicationContext context;

    @Test
    public void corsEnabled() {
        assertThat(this.context.getBean(CorsAutoConfiguration.class)).isNotNull();
    }

}
