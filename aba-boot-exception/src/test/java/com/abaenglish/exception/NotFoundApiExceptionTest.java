package com.abaenglish.exception;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class NotFoundApiExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testNotFoundException() throws NotFoundApiException {

        thrown.expect(NotFoundApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));

        throw new NotFoundApiException(new Exception());
    }

    @Test
    public void testNotFound() throws NotFoundApiException {

        thrown.expect(NotFoundApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));

        throw new NotFoundApiException();
    }

    @Test
    public void testNotFoundMessage() throws NotFoundApiException {

        thrown.expect(NotFoundApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));
        thrown.expect(hasProperty("message", is("NOT FOUND")));

        throw new NotFoundApiException("NOT FOUND");
    }
    
    @Test
    public void testNotFoundCodeMessageObject() throws NotFoundApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR FOUND");

        thrown.expect(NotFoundApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new NotFoundApiException(error);
    }

    @Test
    public void testNotFoundServiceException() throws NotFoundApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR FOUND");

        thrown.expect(NotFoundApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.NOT_FOUND)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new NotFoundApiException(new ServiceException(error));
    }

}
