package com.abaenglish.boot.domain;

import java.io.Serializable;

/**
 * Class that allows all the domain objects to be serializable.
 */
public abstract class DomainObject implements Serializable {

    private static final long serialVersionUID = -3468715681521249432L;

}
