package com.abaenglish.exception.service;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class ServiceExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testServiceExceptionWithMessage() throws ServiceException {

        thrown.expect(ServiceException.class);
        thrown.expect(hasProperty("codeMessage", nullValue()));

        throw new ServiceException("error test message", new Exception());
    }

    @Test
    public void testServiceExceptionWithCodeMessage() throws ServiceException {

        CodeMessage error = new CodeMessage("ERROR", "ERROR FOUND");

        thrown.expect(ServiceException.class);
        thrown.expect(hasProperty("codeMessage", is(error)));

        throw new ServiceException(error);
    }

}
