package com.abaenglish.boot.orika;

public class Person {

    public String name;
    public String lastName;

    public Person(final String name, final String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

}
