package com.abaenglish.boot.autoconfigure.jersey;

import com.abaenglish.boot.autoconfigure.jersey.CorsJerseyAutoConfiguration;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.assertj.core.api.Assertions.assertThat;

public class CorsJerseyAutoConfigurationTest {

    @Test
    public void noOriginHeader() throws Exception {
        CorsJerseyAutoConfiguration filter = new CorsJerseyAutoConfiguration();

        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        mockReq.setServerName("aba.land");
        mockReq.setRequestURI("/");

        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();
        MockFilterConfig mockFilterConfig = new MockFilterConfig();

        filter.init(mockFilterConfig);
        filter.doFilter(mockReq, mockResp, mockFilterChain);
        filter.destroy();

        assertThat(mockResp.getHeader("Access-Control-Allow-Origin")).isNull();
    }

    @Test
    public void withOriginHeader() throws Exception {
        CorsJerseyAutoConfiguration filter = new CorsJerseyAutoConfiguration();

        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        mockReq.setServerName("aba.land");
        mockReq.setRequestURI("/");
        mockReq.addHeader("Origin", "http://localhost:4234");

        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();
        MockFilterConfig mockFilterConfig = new MockFilterConfig();

        filter.init(mockFilterConfig);
        filter.doFilter(mockReq, mockResp, mockFilterChain);
        filter.destroy();

        assertThat(mockResp.getHeader("Access-Control-Allow-Origin")).isEqualTo("http://localhost:4234");
    }

}
