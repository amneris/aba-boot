package com.abaenglish.boot.orika;

import org.springframework.stereotype.Component;

@Component
public class PersonToPersonResponseExcludeMapper extends BaseCustomMapper<Person, PersonResponse> {

    public PersonToPersonResponseExcludeMapper() {
        super();

        addExclude("lastName");
    }

}
