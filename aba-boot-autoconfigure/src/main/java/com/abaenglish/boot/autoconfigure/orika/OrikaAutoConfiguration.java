package com.abaenglish.boot.autoconfigure.orika;

import com.abaenglish.boot.orika.OrikaBeanMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(OrikaBeanMapper.class)
@Import({OrikaBeanMapper.class})
@ComponentScan(basePackages = "com.abaenglish.boot.orika")
public class OrikaAutoConfiguration {

}
