package com.abaenglish.boot.autoconfigure.jersey;

import com.abaenglish.boot.autoconfigure.properties.BuildProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {JerseyApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class JerseyResourceAutoConfigurationIT {

    @Autowired
    AnnotationConfigApplicationContext context;

    @Autowired
    BuildProperties build;

    @Test
    public void jerseyResourceAutoConfiguration() {
        assertThat(context.getBeanNamesForType(JerseyResourceAutoConfiguration.class).length).isEqualTo(1);
    }

    @Test
    public void canBindAllProperties() {
        assertThat(build.getArtifact()).isEqualTo("artifact-test");
        assertThat(build.getName()).isEqualTo("name-test");
        assertThat(build.getDescription()).isEqualTo("description-test");
        assertThat(build.getVersion()).isEqualTo("1.0.0-SNAPSHOT");
    }

}
