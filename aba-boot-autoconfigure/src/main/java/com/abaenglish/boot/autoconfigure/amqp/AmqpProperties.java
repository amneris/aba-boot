package com.abaenglish.boot.autoconfigure.amqp;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Optional;

@ConfigurationProperties(prefix = "amqp")
public class AmqpProperties {

    private Boolean dummy;
    private Optional<String> exchange;
    private List<Queue> queues;
    private Retry retry;

    public Boolean getDummy() {
        return dummy;
    }

    public void setDummy(Boolean dummy) {
        this.dummy = dummy;
    }

    public Optional<String> getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = Optional.of(exchange);
    }

    public List<Queue> getQueues() {
        return queues;
    }

    public void setQueues(List<Queue> queues) {
        this.queues = queues;
    }

    public Retry getRetry() {
        return retry;
    }

    public void setRetry(Retry retry) {
        this.retry = retry;
    }

    public static class Queue {
        private String name;
        private String exchange;
        private List<String> routingKeys;
        private Integer timeout;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getExchange() {
            return exchange;
        }

        public void setExchange(String exchange) {
            this.exchange = exchange;
        }

        public List<String> getRoutingKeys() {
            return routingKeys;
        }

        public void setRoutingKeys(List<String> routingKeys) {
            this.routingKeys = routingKeys;
        }

        public Integer getTimeout() {
            return timeout;
        }

        public void setTimeout(Integer timeout) {
            this.timeout = timeout;
        }
    }

    public static class Retry {
        private BackOff backOff;
        private int maxAttempts;

        public BackOff getBackOff() {
            return backOff;
        }

        public void setBackOff(BackOff backOff) {
            this.backOff = backOff;
        }

        public int getMaxAttempts() {
            return maxAttempts;
        }

        public void setMaxAttempts(int maxAttempts) {
            this.maxAttempts = maxAttempts;
        }
    }

    public static class BackOff {
        private Long initialInterval;
        private Long multiplier;
        private Long maxInterval;

        public Long getInitialInterval() {
            return initialInterval;
        }

        public void setInitialInterval(Long initialInterval) {
            this.initialInterval = initialInterval;
        }

        public Long getMultiplier() {
            return multiplier;
        }

        public void setMultiplier(Long multiplier) {
            this.multiplier = multiplier;
        }

        public Long getMaxInterval() {
            return maxInterval;
        }

        public void setMaxInterval(Long maxInterval) {
            this.maxInterval = maxInterval;
        }
    }
}
