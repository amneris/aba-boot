package com.abaenglish.boot.exception;

import java.io.Serializable;

public class CodeMessage implements Serializable {

    private static final long serialVersionUID = -2798557200909797874L;

    private String code;
    private String message;

    public CodeMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

