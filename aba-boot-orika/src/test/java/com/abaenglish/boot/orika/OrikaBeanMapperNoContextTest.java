package com.abaenglish.boot.orika;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OrikaBeanMapperNoContextTest {

    private static final Person PERSON = new Person("Name", "Last");

    private OrikaBeanMapper mapper;

    @Before
    public void setUp() {
        mapper = new OrikaBeanMapper(true);
    }

    @Test
    public void orikaBeanMapperEmpty() {
        PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

        assertThat(personResponse.firstName).isNull();
        assertThat(personResponse.lastName).isEqualTo("Last");
    }

    @Test
    public void mapperWithFields() {
        mapper.addMapper(new PersonToPersonResponseFieldMapper());

        PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

        assertThat(personResponse.firstName).isEqualTo("Name");
        assertThat(personResponse.lastName).isEqualTo("Last");
    }

}
