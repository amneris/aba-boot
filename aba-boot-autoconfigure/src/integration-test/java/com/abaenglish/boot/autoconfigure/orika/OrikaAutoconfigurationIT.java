package com.abaenglish.boot.autoconfigure.orika;

import com.abaenglish.boot.orika.converter.OrikaLocalDateConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {OrikaAutoConfiguration.class}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class OrikaAutoconfigurationIT {
    @Autowired
    AnnotationConfigApplicationContext context;

    @Test
    public void orikaAutoConfiguration() {
        assertThat(context.getBeanNamesForType(OrikaLocalDateConverter.class).length).isEqualTo(1);
    }
}
