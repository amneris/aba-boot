package com.abaenglish.exception;

import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.ForbiddenApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class ForbiddenApiExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testForbiddentException() throws ForbiddenApiException {

        thrown.expect(ForbiddenApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.FORBIDDEN)));

        throw new ForbiddenApiException(new Exception());
    }

    @Test
    public void testForbiddent() throws ForbiddenApiException {

        thrown.expect(ForbiddenApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.FORBIDDEN)));

        throw new ForbiddenApiException();
    }

    @Test
    public void testForbiddentMessage() throws ForbiddenApiException {

        thrown.expect(ForbiddenApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.FORBIDDEN)));
        thrown.expect(hasProperty("message", is("FORBIDDEN")));

        throw new ForbiddenApiException("FORBIDDEN");
    }
    
    @Test
    public void testForbiddentCodeMessageObject() throws ForbiddenApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR FORBIDDEN");

        thrown.expect(ForbiddenApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.FORBIDDEN)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new ForbiddenApiException(error);
    }

    @Test
    public void testForbiddentWithServiceException() throws ForbiddenApiException {

        CodeMessage error = new CodeMessage("", "");
        error.setCode("ERROR");
        error.setMessage("ERROR FORBIDDEN");

        thrown.expect(ForbiddenApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.FORBIDDEN)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new ForbiddenApiException(new ServiceException(error));
    }

}
