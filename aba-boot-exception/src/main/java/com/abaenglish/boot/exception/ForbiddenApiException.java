package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class ForbiddenApiException extends ApiException {

    private static final long serialVersionUID = -5799499038519523231L;

    public ForbiddenApiException(Exception ex) {
        super(ex, HttpStatus.FORBIDDEN);
    }

    public ForbiddenApiException() {
        super(HttpStatus.FORBIDDEN);
    }

    public ForbiddenApiException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }

    public ForbiddenApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.FORBIDDEN);
    }

    public ForbiddenApiException(ServiceException ex) {
        super(ex, HttpStatus.FORBIDDEN);
    }

}