package com.abaenglish.boot.autoconfigure.springfox;

import com.abaenglish.boot.autoconfigure.properties.BuildProperties;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@ConditionalOnClass(Docket.class)
@EnableConfigurationProperties(BuildProperties.class)
@EnableSwagger2
public class SpringfoxAutoConfiguration {

    @Autowired
    private BuildProperties properties;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class)).paths(PathSelectors.any()).build()
                .pathMapping("/")
                .apiInfo(new ApiInfoBuilder()
                        .title(properties.getName())
                        .description(properties.getDescription())
                        .version(properties.getVersion())
                        .build());
    }

}
