package com.abaenglish.boot.exception;

import com.abaenglish.boot.exception.service.ServiceException;
import org.springframework.http.HttpStatus;

public class ConflictApiException extends ApiException {

    private static final long serialVersionUID = -2771899316397310717L;

    public ConflictApiException(Exception ex) {
        super(ex, HttpStatus.CONFLICT);
    }

    public ConflictApiException() {
        super(HttpStatus.CONFLICT);
    }

    public ConflictApiException(String message) {
        super(message, HttpStatus.CONFLICT);
    }

    public ConflictApiException(CodeMessage codeMessage) {
        super(codeMessage, HttpStatus.CONFLICT);
    }

    public ConflictApiException(ServiceException ex) {
        super(ex, HttpStatus.CONFLICT);
    }

}
