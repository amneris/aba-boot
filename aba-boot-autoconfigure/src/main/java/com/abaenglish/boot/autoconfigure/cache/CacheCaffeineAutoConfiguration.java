package com.abaenglish.boot.autoconfigure.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
@ConditionalOnClass({ Caffeine.class })
@ConditionalOnProperty(prefix = "spring.cache", name = "enabled", havingValue = "true")
@AutoConfigureAfter(name = { "CacheAutoConfiguration", "CaffeineCacheConfiguration" })
public class CacheCaffeineAutoConfiguration {

}
