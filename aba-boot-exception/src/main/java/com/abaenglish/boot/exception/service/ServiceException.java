package com.abaenglish.boot.exception.service;

import com.abaenglish.boot.exception.CodeMessage;

public class ServiceException extends Exception {

    private static final long serialVersionUID = -6621310346981302999L;

    private final CodeMessage codeMessage;

    public ServiceException(String message, Throwable t) {
        super(message, t);
        this.codeMessage = null;
    }

    @Deprecated
    public ServiceException(CodeMessage codeMessage) {
        super(codeMessage.getMessage());
        this.codeMessage = codeMessage;
    }

    public ServiceException(CodeMessage codeMessage, Throwable cause) {
        super(codeMessage.getMessage(),cause);
        this.codeMessage = codeMessage;
    }

    public CodeMessage getCodeMessage() {
        return codeMessage;
    }

}
