package com.abaenglish.exception;

import com.abaenglish.boot.exception.BadRequestApiException;
import com.abaenglish.boot.exception.CodeMessage;
import com.abaenglish.boot.exception.service.ServiceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.HttpStatus;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;

public class BadRequestApiExceptionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testBadRequestException() throws BadRequestApiException {

        thrown.expect(BadRequestApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.BAD_REQUEST)));

        throw new BadRequestApiException(new Exception());
    }

    @Test
    public void testBadRequest() throws BadRequestApiException {

        thrown.expect(BadRequestApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.BAD_REQUEST)));

        throw new BadRequestApiException();
    }

    @Test
    public void testBadRequestMessage() throws BadRequestApiException {

        thrown.expect(BadRequestApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.BAD_REQUEST)));
        thrown.expect(hasProperty("message", is("NOT FOUND")));

        throw new BadRequestApiException("NOT FOUND");
    }

    @Test
    public void testBadRequestCodeMessageObject() throws BadRequestApiException {

        CodeMessage error = new CodeMessage("ERROR", "ERROR FOUND");

        thrown.expect(BadRequestApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.BAD_REQUEST)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new BadRequestApiException(error);
    }

    @Test
    public void testBadRequestWithServiceException() throws BadRequestApiException {

        CodeMessage error = new CodeMessage("ERROR", "ERROR FOUND");

        thrown.expect(BadRequestApiException.class);
        thrown.expect(hasProperty("httpStatus", is(HttpStatus.BAD_REQUEST)));
        thrown.expect(hasProperty("message", is(error.getMessage())));
        thrown.expect(hasProperty("code", is(error.getCode())));

        throw new BadRequestApiException(new ServiceException(error));
    }

}
