package com.abaenglish.boot.autoconfigure.logging;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class DynamicLogLevelAutoConfigurationIT {

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = {DynamicLogLevelAutoConfiguration.class}, properties = {"logging.dynamicLogLevel = enable"}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
    public static class PropertyTrueIT {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void propertyTrue() {
            assertThat(context.getBeanNamesForType(ThreadLoggingFilterBean.class).length).isEqualTo(1);
            assertThat(context.getBeanNamesForType(ThreadLoggingInitializer.class).length).isEqualTo(1);
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest(classes = {DynamicLogLevelAutoConfiguration.class}, properties = {"logging.dynamicLogLevel = disable"}, webEnvironment = SpringBootTest.WebEnvironment.NONE)
    public static class PropertyFalseIT {

        @Autowired
        AnnotationConfigApplicationContext context;

        @Test
        public void propertyFalse() {
            assertThat(context.getBeanNamesForType(ThreadLoggingFilterBean.class).length).isEqualTo(0);
            assertThat(context.getBeanNamesForType(ThreadLoggingInitializer.class).length).isEqualTo(0);
        }

    }

}
