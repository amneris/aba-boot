package com.abaenglish.boot.autoconfigure.logging;

import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;

public class ThreadLoggingFilterBeanTest {

    @Test
    public void test() throws IOException, ServletException {
        ThreadLoggingFilterBean threadLoggingFilterBean = new ThreadLoggingFilterBean();

        MockHttpServletRequest mockReq = new MockHttpServletRequest();
        mockReq.setServerName("aba.land");
        mockReq.setRequestURI("/");

        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        MockFilterChain mockFilterChain = new MockFilterChain();

        threadLoggingFilterBean.doFilter(mockReq, mockResp, mockFilterChain);
    }

}
