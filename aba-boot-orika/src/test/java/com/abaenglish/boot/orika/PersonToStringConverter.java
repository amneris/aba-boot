package com.abaenglish.boot.orika;

import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Component;

@Component
public class PersonToStringConverter extends CustomConverter<Person, String> {

    @Override
    public String convert(Person person, Type<? extends String> type, MappingContext mappingContext) {
        String name = null;

        if (person != null) {
            name = person.name;
        }
        return name;
    }
}
