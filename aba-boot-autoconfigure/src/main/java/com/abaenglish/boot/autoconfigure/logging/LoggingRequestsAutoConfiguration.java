package com.abaenglish.boot.autoconfigure.logging;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
@ConditionalOnClass(CommonsRequestLoggingFilter.class)
@ConditionalOnProperty(value = "logging.level.org.springframework.web.filter.CommonsRequestLoggingFilter", havingValue = "DEBUG")
public class LoggingRequestsAutoConfiguration {

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter crlf = new CommonsRequestLoggingFilter();
        crlf.setIncludeClientInfo(true);
        crlf.setIncludeQueryString(true);
        crlf.setIncludePayload(false);
        return crlf;
    }

}
