package com.abaenglish.boot.orika;

import com.abaenglish.boot.orika.converter.OrikaLocalDateConverter;
import com.abaenglish.boot.orika.converter.OrikaLocalDateTimeConverter;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Enclosed.class)
public class OrikaBeanMapperTest {

    public static final Person PERSON = new Person("Name", "Last");

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, OrikaBeanMapper.class})
    public static class OrikaBeanMapperEmpty {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void orikaBeanMapperEmpty() {
            PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

            assertThat(personResponse.firstName).isNull();
            assertThat(personResponse.lastName).isEqualTo("Last");
        }

    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, BaseMapper.class, OrikaBeanMapper.class})
    public static class CustomMapper {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void customMapper() {
            PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

            assertThat(personResponse.firstName).isNull();
            assertThat(personResponse.lastName).isEqualTo("Last");
        }

    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, PersonToPersonResponseFieldMapper.class, OrikaBeanMapper.class})
    public static class MapperField {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void mapperWithFields() {
            PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

            assertThat(personResponse.firstName).isEqualTo("Name");
            assertThat(personResponse.lastName).isEqualTo("Last");
        }

    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, PersonToPersonResponseExcludeMapper.class, OrikaBeanMapper.class})
    public static class MapperExcludes {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void mapperWithExcludes() {
            PersonResponse personResponse = mapper.map(PERSON, PersonResponse.class);

            assertThat(personResponse.firstName).isNull();
            assertThat(personResponse.lastName).isNull();
        }

    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, PersonToStringConverter.class, OrikaBeanMapper.class})
    public static class OrikaBeanMapperWithConverter {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void orikaBeanMapperWithConverter() {
            String name = mapper.map(PERSON, String.class);

            assertThat(name).isEqualTo("Name");
        }
    }

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = {OrikaLocalDateConverter.class, OrikaLocalDateTimeConverter.class, OrikaBeanMapper.class})
    public static class OrikaBeanMapperWithDatesConverter {

        @Autowired
        OrikaBeanMapper mapper;

        @Test
        public void orikaBeanMapperWithConverter() {
            final DateA a = new DateA(LocalDate.now(), LocalDateTime.now());

            DateB b = mapper.map(a, DateB.class);

            assertThat(a.localDate).isEqualTo(b.localDate);
            assertThat(a.localDateTime).isEqualTo(b.localDateTime);
        }
    }

}
