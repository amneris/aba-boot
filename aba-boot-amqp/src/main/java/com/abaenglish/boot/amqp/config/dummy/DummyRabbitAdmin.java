package com.abaenglish.boot.amqp.config.dummy;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;

import java.util.Properties;

public class DummyRabbitAdmin implements AmqpAdmin {

    @Override
    public void declareExchange(Exchange exchange) {
        // This method must instantiate a dummy array for mock exchange
    }

    @Override
    public boolean deleteExchange(String exchangeName) {
        return false;
    }

    @Override
    public Queue declareQueue() {
        return new Queue("testing");
    }

    @Override
    public String declareQueue(Queue queue) {
        return null;
    }

    @Override
    public boolean deleteQueue(String queueName) {
        return false;
    }

    @Override
    public void deleteQueue(String queueName, boolean unused, boolean empty) {
        // This method must instantiate a dummy array for mock queues
    }

    @Override
    public void purgeQueue(String queueName, boolean noWait) {
        // This method must clean queue dummy array.
    }

    @Override
    public void declareBinding(Binding binding) {
        // This method must bind queue and exchange arrays.
    }

    @Override
    public void removeBinding(Binding binding) {
        // This method must remove queue and exchange bind.
    }

    @Override
    public Properties getQueueProperties(String queueName) {
        return null;
    }

}
