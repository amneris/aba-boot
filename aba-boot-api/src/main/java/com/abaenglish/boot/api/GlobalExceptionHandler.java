package com.abaenglish.boot.api;

import com.abaenglish.boot.exception.ApiException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public void handleApiException(ApiException e, HttpServletResponse response) throws IOException {

        if (e.getApiMessage() != null) {
            response.sendError(e.getHttpStatus().value(), e.getApiMessage());
        } else {
            response.sendError(e.getHttpStatus().value());
        }

    }

}
