package com.abaenglish.boot.data.repository.dataset;

import com.github.springtestdbunit.dataset.AbstractDataSetLoader;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CustomReplacementDataSetLoader extends AbstractDataSetLoader {
    private Map<String, Object> replacements = new ConcurrentHashMap<>();

    @Override
    protected IDataSet createDataSet(Resource resource) throws Exception {
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        builder.setColumnSensing(true);
        try (InputStream inputStream = resource.getInputStream()) {
            return createReplacementDataSet(builder.build(inputStream));
        }
    }

    private ReplacementDataSet createReplacementDataSet(FlatXmlDataSet dataSet) {
        ReplacementDataSet replacementDataSet = new ReplacementDataSet(dataSet);

        replacementDataSet.addReplacementObject("[null]", null);
        replacementDataSet.addReplacementObject("[NULL]", null);
        replacementDataSet.addReplacementObject("[TODAY]", new Date());
        replacementDataSet.addReplacementObject("[NOW]", new Timestamp(System.currentTimeMillis()));

        for (Map.Entry<String, Object> entry : replacements.entrySet()) {
            replacementDataSet.addReplacementObject("[" + entry.getKey() + "]", entry.getValue());
        }
        replacements.clear();

        return replacementDataSet;
    }

    public void replace(String replacement, Object value) {
        replacements.put(replacement, value);
    }
}
