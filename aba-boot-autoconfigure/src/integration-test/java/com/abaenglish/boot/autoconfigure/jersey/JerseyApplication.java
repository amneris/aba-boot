package com.abaenglish.boot.autoconfigure.jersey;

import com.abaenglish.boot.autoconfigure.properties.BuildProperties;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.jersey.JerseyProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = { BuildProperties.class, JerseyProperties.class, JerseyResourceAutoConfiguration.class, ResourceConfig.class })
public class JerseyApplication {

    public static void main(String[] args) {
        SpringApplication.run(JerseyApplication.class, args);
    }

}
